Openshift Container Platform - Origin
=====================================

OCP origin. Based on version 3.7 upstream.

Instructions
------------

Clone the repository to your workstation.
```
git clone https://sbadakhc@bitbucket.org/sbadakhc/ocp-origin.git
```


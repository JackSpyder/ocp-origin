#!/bin/bash
# Provision Nodes and GlusterFS Bricks

# Import Settings Variables
source settings

# Set Gcloud Project Identifier
set_project(){
gcloud config set project ${PROJECT}     
}

# Create node template passing in: NAME, TYPE
create_template(){
    gcloud compute instance-templates create ${1}-node-template \
        --machine-type=${2} \
        --no-address \
        --metadata=startup-script=\#\!\ /bin/bash$'\n'sudo\ yum\ -y\ install\ bash-completion\ bind-utils\ bridge-utils\ centos-release-openshift-origin\ dos2unix\ git\ htop\ iftop\ iptables-services\ kexec-tools\ mlocate\ net-tools\ origin-clients\ psacct\ sos\ tree\ wget$'\n'sudo\ yum\ -y\ update \
        --can-ip-forward \
        --maintenance-policy=MIGRATE \
        --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
        --min-cpu-platform=Automatic \
        --tags ${1},http-server,https-server \
        --no-address \
        --image=${IMAGE} \
        --image-project=${PRJ_IMG} \
        --boot-disk-size=64GB \
        --boot-disk-type=pd-standard    
    
    echo " ${1} Node Template Created "
}

# Create a basic health check for the load blancer
create_health_check(){
    gcloud compute --project ${PROJECT} http-health-checks create basic-check \
        --description "Basic Health Check" \
        --port 80 \
        --request-path "/" \
        --check-interval 20 \
        --timeout 10 \
        --unhealthy-threshold 3 \
        --healthy-threshold 2
    
    echo "Basic Health Check Created"
}

# Create a firewall rule for a node group passing in the nodes: NAME
create_firewall_rule(){
    gcloud compute --project=${PROJECT} firewall-rules create allow-${1}-web \
        --description="allow tcp on port 80" \
        --direction=INGRESS \
        --priority=1000 \
        --network=default \
        --action=ALLOW \
        --rules=tcp:80 \
        --target-tags=${1}
    
    echo "allow-${1}-web Firewall Rule Created"
}

# Create a loadbalancer for a node group passing in the nodes: NAME
create_loadbalancer(){
    gcloud compute addresses create ${1}-network-lb \
        --region ${REGION}

    gcloud compute target-pools create ${1}-pool \
        --region ${REGION} \
        --http-health-check basic-check

    gcloud compute forwarding-rules create ${1}-rule \
        --region ${REGION} \
        --ports 80 \
        --address ${1}-network-lb \
        --target-pool ${1}-pool

    gcloud compute forwarding-rules describe ${1}-rule \
        --region ${REGION}

    echo "${1} Load Balancer Created"
}

# Create a node group passing in: NAME
create_group(){
    gcloud compute --project "${PROJECT}" instance-groups managed create "${1}-group-1" \
        --zone ${REGION}-d \
        --base-instance-name ${1}-group-1 \
        --template ${1}-node-template \
        --size 1 \
        --description "${1} Node Group"

    echo "${1} Node Group Created"
}

# Set a group to have auto-scaling passing in: NAME, MIN_COUNT, MAX_COUNT
set_auto_scaling(){
    gcloud compute --project "${PROJECT}" instance-groups managed set-autoscaling "${1}-group-1" \
        --zone ${REGION}-d \
        --cool-down-period 300 \
        --min-num-replicas ${2} \
        --max-num-replicas ${3} \
        --target-cpu-utilization 0.8
    
    echo "${1}-group-1 Auto Scaling Set"
}

# Set a target pool for a loadbalancer passing in group: NAME
set_target_pools(){
    gcloud compute instance-groups managed set-target-pools "${1}-group-1" \
        --target-pools ${1}-pool \
        --zone ${REGION}-d 

    echo "${1}-group-1 Added to ${1}-pool"
}

# Create health check and build bastion template and group. Will initialize 1 instance, no autoscaling
create_health_check
create_template "$BST_NAME" "$BST_TYPE"
create_group "$BST_NAME"

# Create the master cluster including firewall, loadbalancer, template, group, autoscaling and LB pool
create_firewall_rule "$MST_NAME"
create_loadbalancer "$MST_NAME"
create_template "$MST_NAME" "$MST_TYPE"
create_group "$MST_NAME"
set_auto_scaling "$MST_NAME" "$MST_MIN_COUNT" "$MST_MAX_COUNT"
set_target_pools "$MST_NAME"

# Create the GlusterFS cluster including firewall, loadbalancer, template, group, autoscaling and LB pool
create_firewall_rule "$GFS_NAME"
create_loadbalancer "$GFS_NAME"
create_template "$GFS_NAME" "$GFS_TYPE"
create_group "$GFS_NAME"
set_auto_scaling "$GFS_NAME" "$GFS_MIN_COUNT" "$GFS_MAX_COUNT"
set_target_pools "$GFS_NAME"

echo " "
echo "Cluster Created!"
echo " "